<?php include('header.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="hero-area">
    <div class="welcome-bg"><img src="assets/img/slider-bg01.png" alt=""></div>
    <div class="welcome-bg-02"><img src="assets/img/slider-bg02.png" alt=""></div>
    <div class="container">
        <div class="hero-content d-table">
            <div class="hero-txt d-table-cell">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="welcome-txt">
                            <h2>Do not risk your business and your future, work with people who can best explain each of your needs</h2>
                            <a href="#" class="slid-btn">get a quote <i class="icofont icofont-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="welcome-pic">
                            <img class="img-responsive" src="assets/img/slide.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<section id="purpose">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h2></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="purpose-txt">
                        <p class="sec-name">About Sparkbit</p>
                        <h2>Ultimate experiences with Story, emotion and purpose</h2>
                        <p>We help our clients succeed by creating brand identities, digital experiences, and print materials that communicate clearly, achieve marketing goals, and look fantastic.</p>
                        <a href="#" class="slid-btn">Know More<i class="icofont icofont-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="purpose-gallery">
                        <div class="row">
                            <div class="col-lg-2 zero-padding">
                                <div class="purpose-pic">
                                    <img class="pur-z-01" src="assets/img/web01.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-5 zero-padding">
                                <div class="purpose-pic text-right">
                                    <img src="assets/img/web02.png" alt="">
                                </div>
                                <div class="purpose-pic text-right">
                                    <img class="pur-z-02 pur-c2-02" src="assets/img/web05.png" alt="">
                                </div>

                            </div>
                            <div class="col-lg-5 zero-padding">
                                <div class="purpose-pic text-left">
                                    <img class="pur-z-02 pur-c-02" src="assets/img/web03.png" alt="">
                                    <img class="pur-z-06" src="assets/img/web04.png" alt="">
                                </div>
                                <div class="purpose-pic">
                                    <img src="assets/img/web06.png" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->


<!--    [ Strat Section Area]-->
<section id="project">
    <div class="pro-bg"><img src="assets/img/p-bg.png" alt=""></div>
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="project-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="project-pic"><img src="assets/img/p01.jpg" alt=""></div>
                                <div class="project-pic"><img src="assets/img/p02.jpg" alt=""></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="project-pic pro-pic-top"><img src="assets/img/p03.jpg" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="project-txt">
                        <div class="purpose-txt text-right">
                            <p class="sec-name">PORTFOLIO</p>
                            <h2>Some of our latest <br>project</h2>
                            <p>We help our clients succeed by creating brand identities, digital experiences, and print materials that communicate clearly, achieve marketing goals, and look fantastic.</p>
                            <a href="#" class="slid-btn">More Projects <i class="icofont icofont-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<section id="design">
    <div class="design-bg"><img src="assets/img/de-bg.png" alt=""></div>
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="purpose-txt text-center heading-hap">
                        <p class="sec-name">Working Process</p>
                        <h2>We Design, Develop & Deliver</h2>
                        <p>Offering a variety of internet services from basic web design to complete e-commerce store website development and advanced backend database structure.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 bdr-btm">
                    <div class="row">
                        <div class="col-lg-4 zero-padding">
                            <div class="design-pic">
                                <img src="assets/img/de01.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 zero-padding">
                            <div class="design-txt">
                                <h3>Meet</h3>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4 zero-padding">
                            <div class="design-pic design-pic-dashed">
                                <img src="assets/img/de02.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 zero-padding">
                            <div class="design-txt">
                                <h3>Plan</h3>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6  bdr-btm">
                    <div class="row">
                        <div class="col-lg-4 zero-padding">
                            <div class="design-pic design-pic-dashed">
                                <img src="assets/img/de04.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 zero-padding">
                            <div class="design-txt">
                                <h3>Develop</h3>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4 zero-padding">
                            <div class="design-pic">
                                <img src="assets/img/de03.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 zero-padding">
                            <div class="design-txt">
                                <h3>Design</h3>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6  bdr-btm">
                    <div class="row">
                        <div class="col-lg-4 zero-padding">
                            <div class="design-pic">
                                <img src="assets/img/de05.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 zero-padding">
                            <div class="design-txt">
                                <h3>Launch</h3>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4 zero-padding">
                            <div class="design-pic">
                                <img src="assets/img/de06.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 zero-padding">
                            <div class="design-txt">
                                <h3>Support</h3>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<section id="happy">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="purpose-txt text-center heading-hap">
                        <p class="sec-name">Testimonial</p>
                        <h2>They are happy</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="testi-txt text-center">
                        <div class="testi-article">
                            <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
                        </div>
                        <div class="testi-pic">
                            <img src="assets/img/testi01.png" alt="">
                        </div>
                        <div class="testi-name">
                            <p>Mona Lisa <span>Marketing Manager, Cryptwork</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
